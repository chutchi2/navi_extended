payload_name="payload"
temp_payload_src=$(mktemp ./$payload_name.XXXXX)
target="dummy"

#g++ ../dummy.cpp -o dummy -no-pie
oep=$(readelf -h $target | grep -i "entry point address" | grep -o -P '(?<= 0x)(.*?)([\S]+)')
# oep=$(printf "%016d\n" $(readelf -h $target | grep -i "entry point address" | grep -o -P '(?<= 0x)(.*?)([\S]+)'))
# oeplittleendinad=${oep:14:2}${oep:12:2}${oep:10:2}${oep:8:2}${oep:6:2}${oep:4:2}${oep:2:2}${oep:0:2}
echo "oep: $oep"
echo "oeplittleendinad: $oeplittleendinad"
sed "s/<REPLACEWITHOEP>/0x$oep/g" $payload_name.s >> $temp_payload_src


nasm -f bin -o $payload_name.bin $temp_payload_src
nasm -f elf64 -o $payload_name.elf64 $temp_payload_src
ld -o $payload_name $payload_name.elf64 -m elf_x86_64
touch $temp_payload_src && rm $temp_payload_src
#read -p "Press [Enter] to inject..."

#You can build the elf_inject you see below from https://github.com/mfaerevaag/elfinjector/tree/master
../uf0o/binary_analysis/elf_inject $target $payload_name.bin ".injected" 0x800000 -1
injected_addr=0$(readelf --wide --headers $target | grep ".injected" | grep -i "progbit" | grep -o -P '(?<= 0)(.*?)([\S]+)' -m 1 | head -1)
littleendinad=${injected_addr:14:2}${injected_addr:12:2}${injected_addr:10:2}${injected_addr:8:2}${injected_addr:6:2}${injected_addr:4:2}${injected_addr:2:2}${injected_addr:0:2}
echo "injected_addr: $injected_addr"
echo "littleendinad: $littleendinad"
# read -p "Press [Enter] to change EP..."

#This patcher can be found here https://github.com/uf0o/binary_analysis/tree/master
python3 ../uf0o/binary_analysis/ep_patcher.py $target $littleendinad
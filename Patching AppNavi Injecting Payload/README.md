# How to build the image?

Go to the folder of this project and run on the terminal
```
docker build -t gen5wnavipatcher -f ./dockerfile ./
```

# How to patch AppNavi?

Go to the folder where you have your AppNavi and run the following: 
```
docker run -it -v $PWD:/mnt gen5wnavipatcher
```

It will create a new AppNavi patched to run navi_extended and will also create a bak of your AppNavi before patching, just in case.
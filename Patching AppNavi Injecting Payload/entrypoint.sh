#!/bin/sh
echo "Im doing the work, be patient, takes a couple seconds."
cd /mnt
payload_name="payload"
temp_payload_src=$(mktemp ./$payload_name.XXXXXX)
target="AppNavi"
cp $target $(mktemp ./$target.bak.XXXXXX)

oep=$(readelf -h $target | grep -i "entry point address" | grep -o -P '(?<= 0x)(.*?)([\S]+)')
echo "oep: $oep"
echo "oeplittleendinad: $oeplittleendinad"
sed "s/<REPLACEWITHOEP>/0x$oep/g" /ctx/$payload_name.s >> $temp_payload_src


nasm -f bin -o /ctx/$payload_name.bin $temp_payload_src
# nasm -f elf64 -o /ctx/$payload_name.elf64 $temp_payload_src
# ld -o $payload_name /ctx/$payload_name.elf64 -m elf_x86_64
touch $temp_payload_src && rm $temp_payload_src

/usr/bin/elf_inject $target /ctx/$payload_name.bin ".injected" 0x800000 -1
injected_addr=0$(readelf --wide --headers $target | grep ".injected" | grep -i "progbit" | grep -o -P '(?<= 0)(.*?)([\S]+)' -m 1 | head -1)
littleendinad=${injected_addr:14:2}${injected_addr:12:2}${injected_addr:10:2}${injected_addr:8:2}${injected_addr:6:2}${injected_addr:4:2}${injected_addr:2:2}${injected_addr:0:2}
echo "injected_addr: $injected_addr"
echo "littleendinad: $littleendinad"
python3 /usr/bin/eg_patcher.py $target $littleendinad
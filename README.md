# navi_extended

This app is meant to replace AppNavi temporarily so you can execute things from the headunit.


#How does this work? 
* `navi_extended` is something I wrote that just finds your USB and executes a shell script on it. 

* In order for the headunit to execute our hack, we need to replace the official `AppNavi` application and use my `navi_extended` instead. (Rename `navi_extended` to => `AppNavi` and place it in `/Bin`) 

* Once we gain execution, we can **extract** the encryption keys and the official `AppNavi` in the unit. 

* With the now `official AppNavi` in our possession, we can now **patch** it so it will always call `navi_extended` when anybody calls `AppNavi`. Once it has called `navi_extended` it will go ahead and proceed with the regular `AppNavi` tasks, giving you your maps back.

# Steps to get it working

## WARNING
**Doing this will temporarily replace the official `AppNavi` and thus making your maps not work in the unit. If you finish all the steps you will get your maps + the hack working side by side. If at any point you give up, just reinstall your OS and that will get rid of all you've done.**
----

MAKE SURE YOU HAVE AN OFFICIAL UPDATE ON YOUR USB, AS WE USE IT TO EXTRACT THE NAVIGATION APP!

1. Head over to https://gitlab.com/g4933/gen5w/navi_extended/-/releases and under assets just download `USB Package (includes AppNavi)`, this is a ready to go USB folder structure with the needed files.  
2. Then plug your USB and do this stuff: https://www.youtube.com/watch?v=vU8nG18sFQ4
3. Your device will restart several times! This is normal! Do not unplug the USB until the maps work again and if you go to media > usb device you can play rick roll's song. 


# How to download? 
Go here https://gitlab.com/g4933/gen5w/navi_extended/-/releases and under Assets you will see "AppNavi" download that. 

# Useful Links
## Updating AppNavi
https://youtu.be/vU8nG18sFQ4

## Using vol knob to enter in Engineering Menu
https://youtu.be/vhcPbpYW5Vk


# Old documentation here for legacy

1. Replace AppNavi with our App (`navi_extended`)
2. On the shell script from your USB you will copy the AppNavi from both partitions `/navi` and `/navi2`, the official AppNavi will be on one of those folders (we also have it available because we can decrypt the update package)
3. You will rename `/app/share/AppUpgrade/DecryptToPipe` -> `/app/share/AppUpgrade/DecryptToPipe_OG`
4. You will create a bash script (first line should contain `#!/bin/bash`) and place it on `/app/share/AppUpgrade/DecryptToPipe` (essentially the old real path) that will first check if you have a specific file on your USB (THIS IS IMPORTANT FOR FAILSAFE) and depending on whether the file exists or not you will either call DecryptToPipe_OG and proceed with the regular update procedure, or call another arbitrary script of your choosing to piggyback on the Update Process to trigger your exploit BEFORE any update id handled by the headunit

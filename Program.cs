// See https://aka.ms/new-console-template for more information
using System.Diagnostics;

#if DEBUG
    const string usbPath = "/Volumes/UNTITLED1";
#else
    const string usbPath = "/media/usb";
#endif

string ExecuteBash(string cmd)
{
    var escapedArgs = cmd.Replace("\"", "\\\"");

    var process = new Process()
    {
        StartInfo = new ProcessStartInfo
        {
            FileName = "/bin/bash",
            Arguments = $"-c \"{escapedArgs}\"",
            RedirectStandardOutput = true,
            UseShellExecute = false,
            CreateNoWindow = true,
        }
    };
    process.Start();
    string result = process.StandardOutput.ReadToEnd();
    process.WaitForExit();
    return result;
}

var wideOpenServiceInstaller =
    Directory.GetFiles(usbPath, "install_wideopen_service.sh", SearchOption.AllDirectories).FirstOrDefault();
        
var mainLoop = Directory.GetFiles(usbPath, "main_loop.sh", SearchOption.AllDirectories).First();

if (wideOpenServiceInstaller != null) ExecuteBash(wideOpenServiceInstaller);

while (true)
{
    var _ = ExecuteBash(mainLoop);
    Thread.Sleep(5000);
}

#!/bin/bash

#THIS IS NOT A JOKE, THIS CAN BE DANGEROUS! 
#Well, the worst that can happen is you pretty much break the AppNavi app and will need to reinstall
# We've seen worst shit, but hey, you are warned. If you mess this, you will need to reinstall the whole OS.
# Its mainly just an annoyance more than anything, but you've been warned.

USB_PATH=$(dirname $0)
cd $USB_PATH

printf "\n----- Caller:($(ps -o comm= $PPID)) - PID:($PPID) - At($(date)) -----\n" ;

if [ ! -f "${USB_PATH}/Bin/AppNavi" ]; then
    printf "We couldn't find  [${USB_PATH}/Bin/AppNavi]. We need it as we will replace current AppNavi with that one. We can't proceed without it." ;
    exit 1;
fi


if [ ! -f "$USB_PATH/DONE_RESTORING_APPNAVI_FLAG" ]; then
    touch $USB_PATH/DONE_RESTORING_APPNAVI_FLAG
    #Code goes here
    mount -o remount,rw /navi 2>&1 ;
    ls -la /navi/Bin 2>&1 ;
    # cp ${USB_PATH}/Bin/navi_extended /navi/Bin/navi_extended 2>&1 ;
    cp ${USB_PATH}/Bin/AppNavi /navi/Bin/AppNavi-stg 2>&1 ;
    cp /navi/Bin/AppNavi /navi/Bin/AppNavi-original.bak  2>&1 ;
    pkill -f "AppNavi"  2>&1 ; 
    mv /navi/Bin/AppNavi-stg /navi/Bin/AppNavi  2>&1 ;
    mount -o remount,ro /navi 2>&1 ;
    ls -la /navi/Bin 2>&1 ;
    sync ;

    mount -o remount,rw /navi2 2>&1 ;
    ls -la /navi2/Bin 2>&1 ;
    # cp ${USB_PATH}/Bin/navi_extended /navi2/Bin/navi_extended 2>&1 ;
    cp ${USB_PATH}/Bin/AppNavi /navi2/Bin/AppNavi-stg 2>&1 ;
    cp /navi2/Bin/AppNavi /navi2/Bin/AppNavi-original.bak  2>&1 ;
    pkill -f "AppNavi"  2>&1 ; 
    mv /navi2/Bin/AppNavi-stg /navi2/Bin/AppNavi  2>&1 ;
    mount -o remount,ro /navi2 2>&1 ;
    ls -la /navi2/Bin 2>&1 ;
    sync ;
    reboot ;
fi
#Code goes here

printf "\n----- END Caller:($(ps -o comm= $PPID)) - PID:($PPID) - At($(date)) END -----\n" ;

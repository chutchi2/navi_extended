#!/bin/bash

# THIS ONE IS REALY SERIOUS. FOR REAL. IF YOU BREAK THIS YOU CAN'T UPDATE ANYMORE.

#sh -c /app/share/AppUpgrade/DecryptToPIPE -d /tmp/438f3cea40a948b78985088e23ae6ee5 /media/usb/1/XXX-XXXX/AppUpgrade 0 | dd of=/update/usb/bin/AppUpgrade bs=512

USB_PATH=$(dirname $0)
cd $USB_PATH

printf "\n----- Caller:($(ps -o comm= $PPID)) - PID:($PPID) - At($(date)) -----\n" ;

# I wanna write, so lets just give us permission to do so.
mount -o remount,rw / 2>&1 ;

# We do backups of the original file. Loosing this is a very very very very very bad day
# If we already have a backup done we don't do another backup, this is because we dont ever want to overwrite by accident our backup! 
if [ ! -f "/app/share/AppUpgrade/DecryptToPIPE_OG" ]; then
    cp /app/share/AppUpgrade/DecryptToPIPE /app/share/AppUpgrade/DecryptToPIPE_OG
    cp /app/share/AppUpgrade/DecryptToPIPE $USB_PATH/DecryptToPIPE_OG
fi

#Check if we actually did the backup
if [ ! -f "/app/share/AppUpgrade/DecryptToPIPE_OG" ]; then
    printf "We couldn't find  [/app/share/AppUpgrade/DecryptToPIPE_OG]. Maybe it wasn't copied. We can't proceed. it's not safe." ;
    exit 1;
fi

# Check if we have the FK code in the usb
if [ ! -f "${USB_PATH}/DecryptToPIPE_FK" ]; then
    printf "We couldn't find  [${USB_PATH}/DecryptToPIPE_FK]. Maybe you haven't created it?. We can't proceed. it's not safe." ;
    exit 1;
fi

# Copy the FK code to the internal location
cp ${USB_PATH}/DecryptToPIPE_FK /app/share/AppUpgrade/DecryptToPIPE_FK

# Check that we could copy the file above 
if [ ! -f "/app/share/AppUpgrade/DecryptToPIPE_FK" ]; then
    printf "We couldn't find  [/app/share/AppUpgrade/DecryptToPIPE_FK]. Maybe it wasn't copied. We can't proceed. it's not safe." ;
    exit 1;
fi

# Replace original DecryptToPIPE with our fk
mv /app/share/AppUpgrade/DecryptToPIPE_FK /app/share/AppUpgrade/DecryptToPIPE

# Well, lets leave it as we found it
mount -o remount,ro / 2>&1 ;
sync ;
sleep 10 ; 
/sbin/reboot ;
#Pray to whatever god you follow and check if your USB got the keys



printf "\n----- END Caller:($(ps -o comm= $PPID)) - PID:($PPID) - At($(date)) END -----\n" ;

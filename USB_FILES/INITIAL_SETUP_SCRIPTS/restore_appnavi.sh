#!/bin/bash

USB_PATH=$(dirname $0)/..
SCRIPT_PATH=$(dirname $0)
printf "\n RESTORE APPNAVI: ----- Caller:($(ps -o comm= $PPID)) - PID:($PPID) - At($(date)) -----\n" ;

if [ -f "${USB_PATH}/STATUS_FLAGS/DONE_RESTORING_APPNAVI_FLAG" ]; then
  printf "\n RESTORE APPNAVI: ----- Seems we have restored already! Remove ["${USB_PATH}/STATUS_FLAGS/DONE_RESTORING_APPNAVI_FLAG"] if this is not the case! -----\n" ;
  exit 0 ;
fi

DER_KEY=$(find $USB_PATH -iname "*_key_*.der" | head -n 1)
if [ ! -f "${DER_KEY}" ]; then  
  printf "\n RESTORE APPNAVI: ----- Decryption key not found! Try extracting first! -----\n" ;
  printf "\n RESTORE APPNAVI: ----- Searchd on [${USB_PATH}]  -----\n" ;
  printf "\n RESTORE APPNAVI: ----- Results we got [${DER_KEY}]  -----\n" ;
  exit 1 
fi

FILENAME="appnavi.tar"
mkdir -p ${USB_PATH}/HU
APPNAVI_TAR_PATH=$(find ${USB_PATH}/HU -iname ${FILENAME} | head -n 1)
if [ ! -f "${APPNAVI_TAR_PATH}" ]; then  
  printf "\n RESTORE APPNAVI: ----- We couldn't find [${FILENAME}], we were searching for it on any folder inside [${USB_PATH}/HU] -----\n" ;
  exit 1 ;
fi

APPNAVI_DECRYPTED_PATH=${USB_PATH}/tmp/update_decrypted/${FILENAME%%.*}/
if [ ! -f "${USB_PATH}/STATUS_FLAGS/DONE_DECRYPTING_AND_DECOMPRESSING_APPNAVI_FLAG" ]; then
  mkdir -p ${USB_PATH}/tmp && rm -rf ${USB_PATH}/tmp ;
  mkdir -p ${APPNAVI_DECRYPTED_PATH} 2>&1
  printf "\n RESTORE APPNAVI: ----- Attempting to decrypt the AppNavi.tar and extract its content!. \n" ;
  /app/share/AppUpgrade/DecryptToPIPE -d ${DER_KEY} ${APPNAVI_TAR_PATH} 0 | cat - > ${APPNAVI_DECRYPTED_PATH}/${FILENAME} 2>&1
  APPNAVI_BIN=$(tar -tf ${APPNAVI_DECRYPTED_PATH}/${FILENAME} | grep -i "bin/appnavi" | head -n 1)
  mkdir -p ${APPNAVI_DECRYPTED_PATH}/decompressed/ 2>&1
  tar -C ${APPNAVI_DECRYPTED_PATH}/decompressed/ -xf ${APPNAVI_DECRYPTED_PATH}/${FILENAME} ${APPNAVI_BIN} 2>&1
  touch ${USB_PATH}/STATUS_FLAGS/DONE_DECRYPTING_AND_DECOMPRESSING_APPNAVI_FLAG
  sync ;
fi


# TODO: ENSURE WE HAVE WIDEOPEN SERVICE INSTALLED!!!!!!!!!
if [ ! -f "${APPNAVI_DECRYPTED_PATH}/decompressed/Bin/AppNavi" ]; then
    printf "\n RESTORE APPNAVI: We couldnt find  [${APPNAVI_DECRYPTED_PATH}/decompressed/Bin/AppNavi]. We need it as we will replace current AppNavi with that one. We cant proceed without it." ;
    ls -laR ${APPNAVI_DECRYPTED_PATH}/ 2>&1
    exit 1;
fi

printf "\n RESTORE APPNAVI: ----- Swapping the patched AppNavi with the one from the update package. \n" ;
if [ ! -f "${USB_PATH}/STATUS_FLAGS/DONE_RESTORING_APPNAVI_FLAG" ]; then
    #Code goes here
    mount -o remount,rw /navi 2>&1 ;
    ls -la /navi/Bin 2>&1 ;
    cp ${APPNAVI_DECRYPTED_PATH}/decompressed/Bin/AppNavi /navi/Bin/AppNavi-stg 2>&1 ;
    cp /navi/Bin/AppNavi /navi/Bin/AppNavi-original.bak  2>&1 ;
    pkill -f "AppNavi"  2>&1 ; 
    mv /navi/Bin/AppNavi-stg /navi/Bin/AppNavi  2>&1 ;
    mount -o remount,ro /navi 2>&1 ;
    ls -la /navi/Bin 2>&1 ;
    sync ;

    mount -o remount,rw /navi2 2>&1 ;
    ls -la /navi2/Bin 2>&1 ;
    cp ${APPNAVI_DECRYPTED_PATH}/decompressed/Bin/AppNavi /navi2/Bin/AppNavi-stg 2>&1 ;
    cp /navi2/Bin/AppNavi /navi2/Bin/AppNavi-original.bak  2>&1 ;
    pkill -f "AppNavi"  2>&1 ; 
    mv /navi2/Bin/AppNavi-stg /navi2/Bin/AppNavi  2>&1 ;
    mount -o remount,ro /navi2 2>&1 ;
    ls -la /navi2/Bin 2>&1 ;
    touch ${USB_PATH}/STATUS_FLAGS/DONE_RESTORING_APPNAVI_FLAG
    sync ;
    /sbin/reboot ;
fi
printf "\n RESTORE APPNAVI: ----- Caller:($(ps -o comm= $PPID)) - PID:($PPID) - At($(date)) -----\n" ;

#!/bin/bash

USB_PATH=$(dirname $0)/..
SCRIPT_PATH=$(dirname $0)
printf "\n EXTRACT KEYS: ----- Caller:($(ps -o comm= $PPID)) - PID:($PPID) - At($(date)) -----\n" ;

mkdir -p ${USB_PATH}/STATUS_FLAGS
if [ ! -f "${USB_PATH}/STATUS_FLAGS/EXTRACT_KEYS_STAGE_1_FLAG" ]; then
  printf "\n EXTRACT KEYS: ----- Doing STAGE 1 of key extraction (Spoofing DecryptToPIPE)! -----\n" ;
  touch ${USB_PATH}/STATUS_FLAGS/EXTRACT_KEYS_STAGE_1_FLAG
  sync ;
  
  # I wanna write, so lets just give us permission to do so.
  mount -o remount,rw / 2>&1 ;
  
  # We do backups of the original file. Loosing this is a very very very very very bad day
  # If we already have a backup done we don't do another backup, this is because we dont ever want to overwrite by accident our backup! 
  if [ ! -f "/app/share/AppUpgrade/DecryptToPIPE_OG" ]; then
      cp /app/share/AppUpgrade/DecryptToPIPE /app/share/AppUpgrade/DecryptToPIPE_OG
      cp /app/share/AppUpgrade/DecryptToPIPE $USB_PATH/DecryptToPIPE_OG
  fi
  
  #Check if we actually did the backup
  if [ ! -f "/app/share/AppUpgrade/DecryptToPIPE_OG" ]; then
      printf "We couldn't find  [/app/share/AppUpgrade/DecryptToPIPE_OG]. Maybe it wasn't copied. We can't proceed. it's not safe." ;
      exit 1;
  fi
  
  # Check if we have the FK code in the usb
  if [ ! -f "${USB_PATH}/DecryptToPIPE_FK" ]; then
      printf "We couldn't find  [${USB_PATH}/DecryptToPIPE_FK]. Maybe you haven't created it?. We can't proceed. it's not safe." ;
      exit 1;
  fi
  
  # Copy the FK code to the internal location
  cp ${USB_PATH}/DecryptToPIPE_FK /app/share/AppUpgrade/DecryptToPIPE_FK
  
  # Check that we could copy the file above 
  if [ ! -f "/app/share/AppUpgrade/DecryptToPIPE_FK" ]; then
      printf "\n EXTRACT KEYS: We couldn't find  [/app/share/AppUpgrade/DecryptToPIPE_FK]. Maybe it wasn't copied. We can't proceed. it's not safe." ;
      exit 1;
  fi
  
  # Replace original DecryptToPIPE with our fk
  mv /app/share/AppUpgrade/DecryptToPIPE_FK /app/share/AppUpgrade/DecryptToPIPE
  
  # Well, lets leave it as we found it
  mount -o remount,ro / 2>&1 ;
  sync ;
  sleep 10 ; 
  /sbin/reboot ;
  
  # Ensure we exit! The system should reboot tho!
  exit 0 ;
fi

if [ ! -f "${USB_PATH}/STATUS_FLAGS/EXTRACT_KEYS_STAGE_2_FLAG" ]; then
  DER_KEY=$(find ${USB_PATH} -iname "*_key_*.der" | head -n 1)
  printf "\n EXTRACT KEYS: ----- Doing STAGE 2 of key extraction (restoring OG DecryptToPIPE)! -----\n" ;
  
  if [ -f "${DER_KEY}" ]; then
    printf "\n EXTRACT KEYS: ----- [$DER_KEY] FOUND! Restoring OG DecryptToPIPE!. -----\n" ;
    # Check if we actually did the backup
    if [ ! -f "/app/share/AppUpgrade/DecryptToPIPE_OG" ]; then
        printf "We couldn't find  [/app/share/AppUpgrade/DecryptToPIPE_OG]. Maybe it wasn't copied. We can't proceed. it's not safe." ;
        exit 1;
    fi
    
    # Flag to check if we have already performed the restore. This is to avoid any race condition and executing the same op twice for some reason.
    if [ ! -f "${USB_PATH}/STATUS_FLAGS/DONE_RESTORING_DECRYPT_OG_FLAG" ]; then
        # Restore original DecryptToPIPE_OG (leaving the backup there anyways)
        printf "We couldn't find  [${USB_PATH}/STATUS_FLAGS/DONE_RESTORING_DECRYPT_OG_FLAG]. Which means we haven't restored yet; we will restore now." ;
        mount -o remount,rw / 2>&1 ;
        cp /app/share/AppUpgrade/DecryptToPIPE_OG /app/share/AppUpgrade/DecryptToPIPE
        mount -o remount,ro / 2>&1 ;
        sync ;
        touch ${USB_PATH}/STATUS_FLAGS/DONE_RESTORING_DECRYPT_OG_FLAG
        touch ${USB_PATH}/STATUS_FLAGS/EXTRACT_KEYS_STAGE_2_FLAG
    fi
    
    printf "\n EXTRACT KEYS: ----- END Caller:($(ps -o comm= $PPID)) - PID:($PPID) - At($(date)) END -----\n" ;
    
    sleep 10 ; 
    /sbin/reboot ;
    exit 0;
  fi
fi

printf "\n EXTRACT KEYS: ----- END OF SCRIPT Caller:($(ps -o comm= $PPID)) - PID:($PPID) - At($(date)) -----\n" ;